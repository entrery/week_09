package ru.edu.task5.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;

@Configuration
public class PostProcessor implements BeanPostProcessor {


    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof InterfaceToWrap){
            return (InterfaceToWrap) () -> ("wrapped " + ((InterfaceToWrap) bean).getValue());
        }
        if (bean instanceof InterfaceToRemove){
            return "";
        }

        return bean;
    }


}
