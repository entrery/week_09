package ru.edu.task5.common;

import org.springframework.stereotype.Component;

/**
 * ReadOnly
 */
@Component
public interface InterfaceToWrap {

    String getValue();

}
