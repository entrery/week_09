package ru.edu.task5.common;

import org.springframework.stereotype.Component;

/**
 * ReadOnly
 */
@Component
public class ImplementOne implements InterfaceToWrap{
    @Override
    public String getValue() {
        return "one";
    }
}
