package ru.edu.task4.java;

import org.springframework.stereotype.Component;

/**
 * ReadOnly
 */
@Component
public interface SomeInterface {
    String getName();
}
