package ru.edu.task1.xml;

import org.springframework.stereotype.Component;

/**
 * ReadOnly
 */

public class ComponentA {

    private ComponentC componentC;

    public ComponentA(ComponentC c) {
        componentC = c;
    }

    public boolean isValid(){
        return componentC != null && componentC.isValid();
    }
}
