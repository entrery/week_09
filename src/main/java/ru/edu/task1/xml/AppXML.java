package ru.edu.task1.xml;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * ReadOnly
 */

public class AppXML {

    public static MainContainer run() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("task_01.xml");
        return context.getBean(MainContainer.class);
    }

}
